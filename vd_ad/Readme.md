## CCU_Video Tasks
  <p align="center">
    This repsoitory contains Valence and Arousal Diarization

<!-- GETTING STARTED -->
## Getting Started
First start by locally importing this repsoitory.
Use terminal commands to clone this repository.
```sh
git clone git@gitlab.com:Vishwanath-parc/parc_ccu_video.git
```
Then move to this directory
```sh
cd parc_ccu_video
```
### Prerequisites

Have conda installed ready, we will create a venv using below

```sh
conda config --append channels conda-forge 
conda create --name <enter venv name here> --file vd_ad/requirements.txt 
 
```
Enter Yes to install pacakges and activate the environment using
```sh
conda activate <enter venv name here>
 
```
### Usage
Once you are inside the environment, run the task using the command

1. Run the python script as follows

```sh
python3 vd_ad/vd_ad.py --input 'vd_ad/test/video.mp4' --output 'vd_ad/test_output'--skipframes 1 
```
* --input  - Input path must be a video file(mp4,avi...). Must be a codec-resolved file.
* --output - Must be a directory path to save the output csv file.
* --skipframes - Enter any integer from 1. Default is 30.



## Documentation - Last update Jan 15 2023

This task reads in a video and predicts continuous valence and arousal values.

Valence is used to indicate the polarity of the emotion. Arousal is used to indicate the intensity of the emotion. Valence and arousal is defined to be an integer between 1 and 1000,with 1 being the lowest and 1000 being the highest.


Source: [Github](https://github.com/bbonik/facial-expression-analysis) 

Associated Papers:
* [V. Vonikakis, D. Neo Yuan Rong, S. Winkler. (2021). MorphSet: Augmenting categorical emotion datasets with dimensional affect labels using face morphing. Proc ICIP2021, Alaska USA, September 2021.](https://arxiv.org/abs/2103.02854)
* [Vonikakis, V., S. Winkler. (2021). Efficient Facial Expression Analysis For Dimensional Affect Recognition Using Geometric Features.](https://arxiv.org/abs/2106.07817)
* [V. Vonikakis, S. Winkler. (2020). Identity Invariant Facial Landmark Frontalization for Facial Expression Analysis. Proc. ICIP2020, Abu Dhabi, October 2020.](https://stefan.winkler.site/Publications/icip2020a.pdf)
 
 
Face Detector : [Haarcascade classifier](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html) from open cv2 

Video Reader : Frames are read using cv2 [VideoCapture Module ](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html)

`Note`: cv2 VideoCapture module can read a saved video or can read a real-time video stream – This can be helpful while integration – when the video frames are passed from ZeroMQ in real-time 

### Project Flow 
1. Read the Video file (Using OpenCV's VideoCapture module)     
`Note`: All the frames in the video are not necessary for model prediction, so we skip frames for faster inference. - Reduced Execution time.
To perform skipping of frames, 
Initialize a count variable before reading the frames and update the count in 
`cap.set(cv2.CAP_PROP_POS_FRAMES, count) `

2. Import haarcascade classified model - For face identification and detection.
This also returns the bounding box co-ordinates of the face region in a frame.

3. Start Processing the video frames
4. Get the fps of the video     
    `Note`: fps of a video is usually around 30. But sometime the videos can be encoded in a different fps. So it is always better to check 
5. Preprocess the frames for image analysis
6. Check if there is atleast one face in a frame.   
    `Note`:  If there are no faces in a frame, the model usually skips those frames during prediction – This might be a problem when matching the time stamps with output frames. So a workaround is a done to output zero for frames with no faces. 

7. Dlib model is executed for prediction
8. Post-processing the results
