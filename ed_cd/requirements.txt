python==3.7
numpy
pandas
dlib
deepface
opencv
scikit-learn==0.23.2
