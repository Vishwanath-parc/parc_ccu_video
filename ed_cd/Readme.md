## CCU_Video Tasks
  <p align="center">
    This repsoitory contains Emotion Detection and Change Detection models.

<!-- GETTING STARTED -->
## Getting Started
First start by locally importing this repsoitory.
Use terminal commands to clone this repository.
```sh
git clone git@gitlab.com:Vishwanath-parc/parc_ccu_video.git
```
Then move to this directory
```sh
cd parc_ccu_video
```
### Prerequisites

List of libraries to install
* deepface
* dlib
* opencv
* scikit-learn
* python 3.7

```sh
pip install -r ed_cd/requirements.txt
```

### Usage

1. Run the python script as follows

```sh
python3 ed_cd/ed_cd.py --input 'ed_cd/test/video_short.mp4' --output 'ed_cd/test_output'  --task ed --skipframes 1 --labeltype top1
```
* --input  - Input path must be a video file(mp4,avi...). Must be a codec-resolved file.
* --output - Must be a directory path to save the output csv file.
* --task - To perform emotion detection(ed) or change detection(cd)
* --skipframes - Enter any integer from 1. Default is 30.
* --labeltype - Model to output top1 or top3 emotion labels.


## Documentation - Last update Jan 15 2023

This task reads in a video and predicts emotions based on [Ekman's Emotion wheel](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiq3LOs4r38AhV3ElkFHRBDAacQFnoECAoQAQ&url=https%3A%2F%2Fwww.frontiersin.org%2Farticles%2F10.3389%2Ffpsyg.2019.00781%2Ffull&usg=AOvVaw005fX56PRHzkOZPOvGqymO) 

6 Basic Emotions by Paul Ekman - Happiness, Sadness, Anger, Fear, Disgust and Surprise. 

Source: [DeepFace](https://viso.ai/computer-vision/deepface/) 

Github: https://github.com/serengil/deepface 
 
DeepFace is the most lightweight face recognition and facial attribute analysis library for Python. The open-sourced DeepFace library includes all leading-edge AI models for face recognition and automatically handles all procedures for facial recognition in the background. 
 
Face Detector : [Haarcascade classifier](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html) from open cv2 

Video Reader : Frames are read using cv2 [VideoCapture Module ](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html)

`Note`: cv2 VideoCapture module can read a saved video or can read a real-time video stream – This can be helpful while integration – when the video frames are passed from ZeroMQ in real-time 

### Project Flow 
1. Read the Video file (Using OpenCV's VideoCapture module)     
`Note`: All the frames in the video are not necessary for model prediction, so we skip frames for faster inference. - Reduced Execution time.
To perform skipping of frames, 
Initialize a count variable before reading the frames and update the count in 
`cap.set(cv2.CAP_PROP_POS_FRAMES, count) `

2. Import haarcascade classified model - For face identification and detection.
This also returns the bounding box co-ordinates of the face region in a frame.

3. Start Processing the video frames
4. Get the fps of the video     
    `Note`: fps of a video is usually around 30. But sometime the videos can be encoded in a different fps. So it is always better to check 
5. Preprocess the frames for image analysis
6. Check if there is atleast one face in a frame.   
    `Note`:  If there are no faces in a frame, the model usually skips those frames during prediction – This might be a problem when matching the time stamps with output frames. So a workaround is a done to output zero for frames with no faces. 

7. DeepFace model is executed for prediction
8. Post-processing the results
