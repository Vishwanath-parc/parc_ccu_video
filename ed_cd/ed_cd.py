"""fer.py: Reads in video and send out predicted emotion."""

__author__ = "Vishwa Chidambaram Ayyappan"
__copyright__ = "Copyright (C) 2022, Xerox PARC 3.0 (a.k.a., PARC Innovations, PARC, PARC, a Xerox company)"
__credits__ = []
__license__ = "APACHE"
__version__ = "0.1"
__maintainer__ = "Vishwa Chidambaram Ayyappan"
__email__ = "vichidamba@parc.com"
__status__ = "Research"

## Conatiner Imports
import os
import sys
import numpy as np
import argparse
from datetime import datetime
from datetime import timedelta
from deepface import DeepFace
from scipy.ndimage import gaussian_filter1d
import pandas as pd
import cv2

## CCU Imports
# import zmq
# from ccu import CCU
# import _thread
# import time



## Main Function
def get_emotion_labels(input="", output="",task="",skipframes="",labeltype=""):
    input_location=input # Input video file
    clip_name = input_location.rsplit('/', 1)[1].split('.')[0] # Remove the input file name extension 
    output_location_ed = output + '/' + clip_name + '_ED.csv' # Output Location must be a directory
    output_location_cd = output + '/' + clip_name + '_CD.csv' # Output Location must be a directory


    # Import face detector model from cv2
    print('Importing haarcascade classified model')
    face_cascade_name = cv2.data.haarcascades + 'haarcascade_frontalface_alt.xml'  # Getting a haarcascade xml file
    face_cascade = cv2.CascadeClassifier()  #processing it for our project
    if not face_cascade.load(cv2.samples.findFile(face_cascade_name)):  #adding a fallback event
        print("Error loading xml file")
    print('Loaded haarcascade model succesfully')

    # Start Video Reader

    VIDEO_STREAM = input_location
    cap = cv2.VideoCapture(VIDEO_STREAM) # Starts processing Video frames

    # Get the fps of the video
    fps = cap.get(cv2.CAP_PROP_FPS)
    print('FPS of the input file:',fps)
    # Get total frame_count of the video
    frames = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    print('Total Frame count:',frames)

    seconds = round(frames / fps)
    video_time = timedelta(seconds=seconds)
    print(f"Video duration in seconds: {seconds}")
    print(f"video time: {video_time}")

    # Initialize emotions dictionary
    ls_joy=[]
    ls_fear=[]
    ls_sadness=[]
    ls_anticipation=[]
    ls_trust=[]
    ls_disgust=[]
    ls_anger=[]
    ls_surprise=[]

    count=0

    # Start Processing frames

    while True:
        (grabbed, frame) = cap.read()  

        if grabbed: # If a frame is present
            """
            This count variable does the skipping of frames. Check the fps of the input file and enter that above.
            """
            cap.set(cv2.CAP_PROP_POS_FRAMES, count)
            count += int(skipframes) # i.e. at 30 fps, this advances one second. So every 30th frame, starting at 0th frame will be read.
        
        if not grabbed: # If not frames left to process
            print ("Finished Reading all frames.")
            break;

        # Convert frames fromm RGB to grayscale for processing
        gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY) #Convert frames to grayscale images
        # Detect faces in the frame using the loaded haarcasacde model
        face=face_cascade.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5)

    
        if len(face)>0: # If there is atleast one face in the frame
            for x,y,w,h in face: # Get the bounding box co-ordinates of the face
                img=cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),1) # save the bounding box face as an image

                try:
                    analyze = DeepFace.analyze(img_path=frame,actions=['emotion']) #DeepFace.analyze does emotion prediction on these faces
                    joy=analyze['emotion']['happy']
                    fear=analyze['emotion']['fear']
                    surprise=analyze['emotion']['surprise']
                    sadness=analyze['emotion']['sad']
                    disgust=analyze['emotion']['disgust']
                    anger=analyze['emotion']['angry']
                    anticipation=0
                    trust=0
  
                except:
                    break

        if len(face)==0: # If there are no faces in a frame to process, then add zero to all emotion dictionary for that frame,
            joy=0
            fear=0
            sadness=0
            anticipation=0
            trust=0
            disgust=0
            anger=0
            surprise=0

        # Append buffer list
        ls_joy.append(joy)
        ls_fear.append(fear)
        ls_sadness.append(sadness)
        ls_anticipation.append(anticipation)
        ls_trust.append(trust)
        ls_disgust.append(disgust)
        ls_anger.append(anger)
        ls_surprise.append(surprise)

    # Joining output labels and their list into a dictionary
    ks = ["joy", "fear","sadness","anticipation", "trust","disgust","anger", "surprise"]
    vals = [ls_joy,ls_fear,ls_sadness,ls_anticipation,ls_trust,ls_disgust,ls_anger,ls_surprise]
    output_dict = dict(zip(ks, vals))
    new = pd.DataFrame.from_dict(output_dict) # Read the dictionary as a dataframe(pandas)
    new['Output_Label']=np.nan # Initalize output label column
    new['Output_Label']=new[["joy", "fear","sadness","anticipation","trust","disgust","anger","surprise"]].idxmax(axis=1) # Output label is the column with max value in this row  
    new['time stamp']=np.nan # Adding time stamp
    for idx in new.index:
        new['time stamp'][idx]=convert_delta(timedelta(seconds=(idx*int(skipframes) /round(fps)))) ### seconds=idx/(fps) Here default fps is 30,  we are skipping 30 frames or 1 second
    new['Seconds'] = new['time stamp'].apply(seconder)
    temp=new[['anger','fear','sadness','anticipation','trust','disgust','joy','surprise']]

    if task=="cd":
            # Change detection code
            """
            This part can be run independently without running the video processing again.
            If the video is already processed with ed_out.csv output. Then take that csv file and run the following data manipulation to get cd_out.csv
            """
            temp2=temp.apply(lambda x:norm(100,0,1,0,x))
            temp2['positive'] = temp2[['joy', 'surprise','anticipation','trust']].mean(axis=1)
            temp2['negative']= temp2[['anger','disgust','sadness','fear']].mean(axis=1)
            temp2['out']=0*temp2['positive']
            temp2['out'] = temp2.apply(lambda x: x['positive'] if x['positive']>x['negative'] else -1*x['negative'], axis=1)
            arr=(np.transpose(np.array(temp2['out']))).reshape(temp2.shape[0],)
            smooth_arr=gaussian_filter1d(temp2,100)
            smooth_arr2 = np.gradient(np.gradient(smooth_arr))
            infls_arr = np.where(np.diff(np.sign(smooth_arr2)))[0]
            temp2.reset_index(inplace=True)
            temp2 = temp2.rename(columns = {'index':'Count'})
            temp_arr = (np.transpose(np.array(temp2['Count']))).reshape(temp2.shape[0],)
            contained = [a in infls_arr for a in temp_arr]
            temp2['Inflection Points']=np.array(contained).tolist()
            temp2['Seconds']=new['Seconds']
            cd_out=temp2[['Seconds','Inflection Points']]

            cd_out.to_csv(output_location_cd,index=False)
    else:
        if labeltype=="top1":
            Tops =pd.DataFrame(temp.apply(lambda x:list(temp.columns[np.array(x).argsort()[::-1][:1]]), axis=1).to_list(),  columns=['Top label'])
        else:
            Tops =pd.DataFrame(temp.apply(lambda x:list(temp.columns[np.array(x).argsort()[::-1][:3]]), axis=1).to_list(),  columns=['Label1', 'Label2', 'Label3']) # Taking the top 3 emotion lables at any given frame
        final=new[['Seconds','Output_Label']]
        ed_out=final.drop('Output_Label',axis=1)
        ed_out = ed_out.join(Tops)

        ed_out.to_csv(output_location_ed,index=False)
        
    print(task+'Task Executed')




# Helper functions
def convert_delta(dlt: timedelta) -> str:
        minutes, seconds = divmod(int(dlt.total_seconds()), 60)
        return f"{minutes}:{seconds:02}"

def seconder(x):
    mins, secs = map(float, x.split(':'))
    td = timedelta(minutes=mins, seconds=secs)
    return td.total_seconds()

def parse_opt():
    parser = argparse.ArgumentParser(description='parc video emotion recognition service')
    parser.add_argument('--input', type=str, default='', help='input video path')
    parser.add_argument('--output', type=str, default='', help='output directory')
    parser.add_argument('--task', type=str, default='', help='ED or CD')
    parser.add_argument('--skipframes', type=int, default=30, help='Number of frames to skip while processing the video, default is 30. Best to keep at this')
    parser.add_argument('--labeltype', type=str, default="", help='top1 or top3 emotion outputs, enter top1 or top3')
    opt = parser.parse_args()
    return opt

def norm(OldMax,OldMin,NewMax,NewMin,OldValue):
  OldRange = (OldMax - OldMin)
  if (OldRange == 0):
      NewValue = NewMin
  else:
    NewRange = (NewMax - NewMin)
    NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
  return NewValue

def main(opt):
    get_emotion_labels(**vars(opt))



if __name__ == "__main__":
    opt = parse_opt()
    main(opt)




